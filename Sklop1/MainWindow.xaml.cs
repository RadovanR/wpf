﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sklop1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ViewModel vm = new ViewModel();
            this.DataContext = vm;
            vm.oglasi.Add(new Oglas("Tamic oglas", "TAM"));
            vm.oglasi.Add(new Oglas("Lada niva", "LADA"));
            vm.oglasi.Add(new Oglas("Yugo 45", "Zastava"));
            LV1.MouseDoubleClick += new MouseButtonEventHandler(PrikaziItem);
            LV1.Items.Refresh();
        }

        private void PrikaziItem(object sender, MouseButtonEventArgs e)
        {
            if (LV1.SelectedItem != null)
            {
                MessageBox.Show($"Izbrali ste vnos z imenom: {LV1.SelectedItem.ToString()}", 
                    "Ime vnosa", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Update(Object sender, RoutedEventArgs routedEventArgs)
        {
            var vm = (ViewModel)this.DataContext;
            if (vm.UpdateItem.CanExecute(null))
            {
                vm.UpdateItem.Execute(null);
                LV1.Items.Refresh();   
            }
            // vm.Izbran = (Oglas)LV1.SelectedItem;
            // vm.NazivDodan = vm.Izbran.Naziv;
            // vm.ZnamkaDodana = vm.Izbran.Znamka;
        }
        private void Izhod(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }
    }
}
