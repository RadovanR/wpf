﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Sklop1
{
    public class ViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Oglas> oglasi;
        public ObservableCollection<Oglas> Oglasi
        {
            get { return oglasi; }
            set { oglasi = value; }
        }
        public ViewModel()
        {
            Oglasi = new ObservableCollection<Oglas>();
        }
        protected void OnPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private Oglas izbran;

        public Oglas Izbran 
        { 
            get { return izbran; } 
            set 
            {
                if (izbran != value)
                {
                    izbran = value;
                    OnPropertyChange(nameof(izbran));
                }
            } 
        }

        private String nazivDodan;
        private String znamka;
        
        public String NazivDodan
        {
            get { return nazivDodan;}
            set
            {
                if (nazivDodan != value)
                {
                    nazivDodan = value;
                    OnPropertyChange(nameof(nazivDodan));
                }
            } 
        }
        
        public String ZnamkaDodana
        {
            get { return znamka;}
            set
            {
                if (znamka != value)
                {
                    znamka = value;
                    OnPropertyChange(nameof(znamka));
                }
            } 
        }

        public ICommand AddItem => new RelayCommand(AddCar);
        public ICommand DeleteItem => new RelayCommand(DeleteCar);
        public ICommand UpdateItem => new RelayCommand(UpdateCar);
        private void AddCar(object o)
        {
            if (NazivDodan == null || ZnamkaDodana == null)
            {
                MessageBox.Show("Vnesi podatke: "+NazivDodan+", "+ZnamkaDodana);
                return;
            }
            Oglasi.Add(new Oglas(NazivDodan, ZnamkaDodana));
        }
        
        private void DeleteCar(object o)
        {
            if (Izbran == null)
            {
                MessageBox.Show("Izberi avto");
                return;
            }
            Oglasi.Remove(Izbran);
        }
        
        private void UpdateCar(object o)
        {
            var oglas1 = Oglasi.IndexOf(Izbran);
            if (Izbran == null)
            {
                MessageBox.Show("Izberi avto");
                return;
            }
            if (NazivDodan == null || ZnamkaDodana == null)
            {
                MessageBox.Show("Vnesi podatke: {0}, {1}"+ NazivDodan, ZnamkaDodana);
                return;
            }
            Izbran.NazivAvta = NazivDodan;
            Izbran.Znamka = ZnamkaDodana;
            OnPropertyChange(nameof(Oglasi));
            OnPropertyChange(nameof(Izbran));
        }
    }
    public class RelayCommand : ICommand
    {
    
        #region Private Members
    
        readonly Action<object> _execute;
        readonly Func<object, bool> _canExecute;
    
        #endregion
        public RelayCommand(Action<object> execute) : this(execute, null) { }
        public RelayCommand(Action<object> execute, Func<object,bool> canExecute) 
        {
            _execute = execute;
            _canExecute = canExecute;
        }
        public bool CanExecute(object parameter)
        {
            if (_canExecute == null)
                return true;
            return _canExecute.Invoke(parameter);
        }
    
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
    }
}
