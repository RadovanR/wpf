﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sklop1
{
    public class Oglas : INotifyPropertyChanged
    {
        
        public event PropertyChangedEventHandler PropertyChanged;
        private string nazivAvta;
        private string znamka;
        private string slika;
        private string kategorija;
        private string pogon;
        private string tipAvta;

        public Oglas()
        {
            
        }
        public Oglas (string naziv, string znamka) {
            this.nazivAvta = naziv;
            this.znamka = znamka;
        }

        public string NazivAvta {
            get
            {
                return nazivAvta;
            }
            set
            {
                if (value.GetType() != typeof(string)) 
                {
                    throw new Exception("Napačen podatkovni tip naziva avta");
                }
                else
                {
                    nazivAvta = value;
                }
            }
        }
        public string Slika
        {
            get { return slika; }
            set {
                if (value.GetType() != typeof(string))
                {
                    throw new Exception("Napačen podatkovni tip slike avta");
                }
                else
                {
                    slika = value;
                }
            }
        }
        public string Znamka
        {
            get { return znamka; }
            set
            {
                if (value.GetType() != typeof(string))
                {
                    throw new Exception("Napačen podatkovni tip znamke avta");
                }
                else
                {
                    znamka = value;
                }
            }
        }
        public string Kategorija
        {
            get { return kategorija; }
            set
            {
                if (value.GetType() != typeof(string))
                {
                    throw new Exception("Napačen podatkovni tip kategorije avta");
                }
                else
                {
                    kategorija = value;
                }
            }
        }
        public string Pogon
        {
            get { return pogon; }
            set
            {
                if (value.GetType() != typeof(string))
                {
                    throw new Exception("Napačen podatkovni tip pogona avta");
                }
                else
                {
                    pogon = value;
                }
            }
        }
        public string TipAvta
        {
            get { return tipAvta; }
            set
            {
                if (value.GetType() != typeof(string))
                {
                    throw new Exception("Napačen podatkovni tip tipa(kategorije) avta");
                }
                else
                {
                    tipAvta = value;
                }
            }
        }

        public override string ToString()
        {
            return $"{nazivAvta}-{znamka}";
        }
        public void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }
    }
}
